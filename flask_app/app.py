import requests
from flask import Flask

app = Flask(__name__)

@app.route('/')
def proxy_hit():
	proxies = {
		"http": "34.93.197.175"
	}
	url = "https://abc.heyweb.site"
	r = requests.get(url, proxies=proxies)
	return r.text


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
